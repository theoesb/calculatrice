print("Bienvenue dans la calculatrice interactive !")
continuer = "oui"

while continuer.lower() == "oui" or continuer.lower() == "o":

    def calcul(operation, chiffre1, chiffre2):
        match operation:
            case "*":
                resultat = chiffre1 * chiffre2
            case "/":
                resultat = chiffre1 / chiffre2
            case "+":
                resultat = chiffre1 + chiffre2
            case "-":
                resultat = chiffre1 - chiffre2
            case _:
                print("Opération non valide")
                resultat = None
        return resultat

    chiffre1 = float(input("Entrée le premier chiffre : "))
    operation = input("Entrée l'opération (+, -, *, /) : ")
    chiffre2 = float(input("Entrée le second chiffre : "))

    resultat = calcul(operation, chiffre1, chiffre2)

    print("Résultat : ", resultat)

    continuer = input("Voulez-vous continuer ? (oui/non) : ")
